#include <QApplication>
#include "maingamewindow.h"
#include <cmath>



int main(int argc, char *argv[]) {
    QApplication   a(argc, argv);

    //MainGameWindow *w = new MainGameWindow(4,2);
    //MainGameWindow *w = new MainGameWindow("xx x\nxx x\nx xx\n");
    MainGameWindow *w = new MainGameWindow(Mode::SQUARE_TOP_LEFT);

    w->show();

    while (w->canMove('W'))
        w->moveRobot('W');


    while (w->canMove('N'))
        w->moveRobot('N');
    //w->moveRobot('N');

    int width = 1;
    w->display(width);
    while (w->canMove('E')) {
        width++;
        w->moveRobot('E');
        w->display(width);
    }


    int height = 1;
    w->display(height);
    while (w->canMove('S')) {
        height++;
        w->moveRobot('S');
        w->display(height);
    }

    w->display("Total: " + QString::number(width * height));
    w->moveRobot('J');

    return a.exec();
}
